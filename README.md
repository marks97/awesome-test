<!-- ABOUT THE PROJECT -->
## About The Project

This is my technical test for 

### Built With

[![Express][Express.com]][Express-url]
[![Node][Nodejs.org]][Nodejs-url]

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

npm, node
  ```sh
  npm install
  npm start
  npm test
  ```

### API

API routes:

- GET /sentence/:id
- GET /sentence/:id/translate
- POST /sentence
- PUT /sentence/:id
- DELETE /sentence/:id

- GET /sentence 
(This route list all the DB data and allows orderBy, order, limit, startAt and endAt parameters).


All the endpoints require an authentification header like the following:

{ "Authorization": "BsNae57Es6N" }

<!-- ROADMAP -->
## Roadmap

- [x] Setup project
- [x] Create scripts for exercise 1 and 2
- [x] Create CRUD endpoints 
- [x] Create unit tests for CRUD endpoints
- [x] Create translator endpoint


<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->
## Contact

Marc Amorós - [my webpage](https://marcamoros.io) - info@marcamoros.io

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[Nodejs.org]: https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white
[Nodejs-url]: https://nodejs.org
[Express.com]: https://img.shields.io/badge/Express.js-404D59?style=for-the-badge
[Express-url]: https://expressjs.com

